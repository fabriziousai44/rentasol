package org.rentasolutions.intake.workOrder;

import org.rentasolutions.intake.vehicle.Vehicle;

import javax.persistence.*;

@Entity
@Table(name = "work_order")
public class WorkOrder {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    Vehicle vehicle;

    public WorkOrder () {

    }

    public WorkOrder(Long id, Vehicle vehicle) {
        this.id = id;
        this.vehicle = vehicle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
