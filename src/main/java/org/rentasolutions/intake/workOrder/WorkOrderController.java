package org.rentasolutions.intake.workOrder;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WorkOrderController {

    public static final String WORK_ORDER_CREATED_SUCCESFULLY = "Work order created succesfully";

    @Autowired
    WorkOrderRepository workOrderRepository;

    @ApiResponse(description = "Retrieve all work orders from the database for a given vin")
    @GetMapping("/workorders/{vin}")
    public List<WorkOrder> createWorkOrder(@PathVariable String vin) {
        return workOrderRepository.findAllByVehicleVin(vin);
    }

    @ApiResponse(description = "Create a work order")
    @PostMapping("/workorders")
    public String createWorkOrder(@RequestBody WorkOrder workOrder) {
        workOrderRepository.save(workOrder);
        return WORK_ORDER_CREATED_SUCCESFULLY;
    }
}
