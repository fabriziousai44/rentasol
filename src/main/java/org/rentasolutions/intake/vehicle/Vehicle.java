package org.rentasolutions.intake.vehicle;

import javax.persistence.*;

@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String vin;
    private String name;
    private String brand;

    public Vehicle() {
    }

    public Vehicle(Long id, String vin, String name, String brand) {
        this.id = id;
        this.vin = vin;
        this.name = name;
        this.brand = brand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    // builder

}
