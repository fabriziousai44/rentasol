package org.rentasolutions.intake.vehicle;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class VehicleController {

    @Autowired
    private VehicleRepository vehicleRepository;

    @ApiResponse(description = "Retrieve all vehicles from the database")
    @GetMapping("/vehicles")
    public List<Vehicle> retrieveAllVehicles() {

        return vehicleRepository.findAll();
    }
}
