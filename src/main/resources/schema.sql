drop table if exists vehicle CASCADE
drop table if exists work_order CASCADE
drop sequence if exists hibernate_sequence
create sequence hibernate_sequence start with 10 increment by 1
create table vehicle (id bigint not null, vin varchar(255), name varchar(255), brand varchar(255), primary key (id), unique(vin))
create table work_order(id bigint not null, vehicle_id bigint, primary key (id))
alter table work_order add constraint FKe7u6wjcct9mnua1l5soqq53q6 foreign key (vehicle_id) references vehicle