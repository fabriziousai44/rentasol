package org.rentasolutions.intake.vehicle;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class VehicleRepositoryTest {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Test
    public void testRetrieveAllVehicles() {

        //given - data.sql injected automatically with Spring Boot

        //when
        List<Vehicle> vehicles = vehicleRepository.findAll();

        //then
        assertThat(vehicles).hasSize(5);
        assertThat(vehicles.get(0).getId()).isEqualTo(1);
        assertThat(vehicles.get(0).getVin()).isEqualTo("4A3AK24FX6E028812");
        assertThat(vehicles.get(0).getName()).isEqualTo("John Doe");
        assertThat(vehicles.get(0).getBrand()).isEqualTo("Mitsubishi");
    }
}

