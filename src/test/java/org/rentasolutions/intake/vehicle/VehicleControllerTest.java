package org.rentasolutions.intake.vehicle;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VehicleController.class)
class VehicleControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    VehicleRepository vehicleRepository;

    @Test
    void testRetrieveAllVehicles() throws Exception {

        //given
        Vehicle vehicle = new Vehicle(1L, "4A3AK24FX6E028812", "John Doe", "Mitsubishi");

        //when
        when(vehicleRepository.findAll()).thenReturn(Arrays.asList(vehicle));
        ResultActions resultActions = mockMvc.perform(get("/vehicles")).andDo(print());

        //then
        resultActions.andExpect(status().isOk()).andExpectAll(
                jsonPath("[0].id", is(vehicle.getId().intValue())),
                jsonPath("[0].vin", is(vehicle.getVin())),
                jsonPath("[0].name", is(vehicle.getName())),
                jsonPath("[0].brand", is(vehicle.getBrand())));
    }
}


