package org.rentasolutions.intake.workOrder;

import org.junit.jupiter.api.Test;
import org.rentasolutions.intake.vehicle.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class WorkOrderRepositoryTest {

    @Autowired
    private WorkOrderRepository workOrderRepository;

    @Test
    public void testRetrieveWorkOrdersForVin() {

        //given - data.sql injected automatically with Spring Boot
        String vin = "4A3AK24FX6E028812";

        //when
        List<WorkOrder> workOrders = workOrderRepository.findAllByVehicleVin(vin);

        //then
        assertThat(workOrders).hasSize(2);
        assertThat(workOrders.get(0).getId()).isEqualTo(1);
        assertThat(workOrders.get(0).getVehicle().getVin()).isEqualTo(vin);
    }

    @Test
    public void testCreateWorkOrderForVin() {

        //given
        Vehicle vehicle = new Vehicle(1L, "4A3AK24FX6E028812", "John Doe", "Mitsubishi");
        WorkOrder workOrder = new WorkOrder();
        workOrder.setVehicle(vehicle);

        //when
        List<WorkOrder> allByVehicleVin = workOrderRepository.findAllByVehicleVin(vehicle.getVin());
        workOrderRepository.save(workOrder);
        List<WorkOrder> allByVehicleVinAfterSave = workOrderRepository.findAllByVehicleVin(vehicle.getVin());

        //then
        assertThat(allByVehicleVin).hasSize(2);
        assertThat(allByVehicleVinAfterSave).hasSize(3);
    }
}
