package org.rentasolutions.intake.workOrder;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.rentasolutions.intake.vehicle.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(WorkOrderController.class)
public class WorkOrderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    WorkOrderRepository workOrderRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testRetrieveAllWorkOrdersWithVin() throws Exception {

        //given
        Vehicle vehicle = new Vehicle(1L, "4A3AK24FX6E028812", "John Doe", "Mitsubishi");
        WorkOrder workOrder = new WorkOrder(1L, vehicle);

        //when
        when(workOrderRepository.findAllByVehicleVin(vehicle.getVin())).thenReturn(Arrays.asList(workOrder));
        ResultActions resultActions = mockMvc.perform(get("/workorders/" + vehicle.getVin())).andDo(print());

        //then
        resultActions.andExpect(status().isOk()).andExpect(
                jsonPath("[0].id", is(1)));
    }

    @Test
    public void testCreateWorkOrderForVin() throws Exception {

        //given
        Vehicle vehicle = new Vehicle(1L, "4A3AK24FX6E028812", "John Doe", "Mitsubishi");
        WorkOrder workOrder = new WorkOrder();
        workOrder.setVehicle(vehicle);

        //when
        ResultActions resultActions = mockMvc.perform(
                post("/workorders").
                        content(objectMapper.writeValueAsString(workOrder)).
                        contentType(MediaType.APPLICATION_JSON)).
                andDo(print());

        //then
        resultActions.andExpect(status().isOk()).andExpect(content().string(WorkOrderController.WORK_ORDER_CREATED_SUCCESFULLY));
    }
}
