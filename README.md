### Exercise

This project contains the code for the exercise. You can use whatever you like in order to complete the exercise. After you finished we will sit together to check what you have done. Don't worry if it's not finished. We will still evaluate of what has been implemented.

_Vehicle_

A vehicle consists of a **unique** vin, a brand and a name. A vehicle can have multiple work orders.

_Work order_

A work order is a request from a driver to a dealer to execute certain activities on a vehicle. It consists of an activity code, a description, a quantity and a price.
e.g. changing winter tyres, changing oil filter, repairing rims.

---

**Objectives**

_Create a RESTful API that satisfies the following requirements_

  - Retrieve all vehicles
  - Retrieve all work orders for a specific vehicle (vin)
  - Create a new work order for a specific vehicle (vin)

_Optional_

  - Add API documentation (OpenAPI Specification)

---

**Build info**

This project uses Gradle as a build tool. The **build.gradle** file is the build configuration script which is used by the gradle command.

`./gradlew build` - compiles, tests, creates jar

`./gradlew test` - runs all tests

---

**Technology Stack**

- Java 11
- Spring Boot
- Spring Data
- H2
- JUnit 5
- Gradle
- Git
- OpenAPI Specification